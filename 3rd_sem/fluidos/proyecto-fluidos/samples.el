;;; -*- mode: emacs-lisp; lexical-binding: t; -*-

(load (expand-file-name (getenv "ORG_EXPORTS_PATH")))

;; (dolist (pkg '(racket-mode))
;;   (unless (package-installed-p pkg)
;;     (package-install pkg)))

;; (require 'racket-mode)

(defun make-suffix-filter (suffix)
  #'(lambda (filename)
      (string-suffix-p suffix filename)))

(setq *org-publish-pdf-notes-filter* (make-suffix-filter "-notes.org"))
(setq *org-publish-pdf-slides-filter* (make-suffix-filter "-slides.org"))

(setq org-publish-project-alist
      `(("notes"
         :base-directory ""
         :base-extension "org"
         :recursive t
         :publishing-directory ""
         :publishing-function org-latex-publish-to-pdf-notes)
        ("slides"
         :base-directory ""
         :base-extension "org"
         :recursive t
         :publishing-directory ""
         :publishing-function org-latex-publish-to-pdf-slides)))

(org-publish-all)
