class Automata:
    def __init__(self, Q, alfabeto, transiciones, q0, F):
        self.q0 = q0
        self.Q = Q
        self.alfabeto = alfabeto
        self.F = F
        self.transiciones = transiciones
    
    def delta(self, estado_actual, caracter):
        if caracter not in self.transiciones[estado_actual]:
            return estado_actual if estado_actual in self.F else (self.transiciones[self.q0][caracter] if caracter in self.transiciones[self.q0] else self.q0)
        return self.transiciones[estado_actual][caracter]

# Ahora, también podríamos modificar la función 'evaluar_automata' para que trabaje con la nueva estructura de clase:
def evaluar_automata(automata, cadena):
    estado_actual = automata.q0
    for caracter in cadena:
        estado_actual = automata.delta(estado_actual, caracter)
    return estado_actual in automata.F

# Crear una instancia de la clase 'Automata' con los mismos datos del diccionario original:
alfabeto = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'}
automata = Automata(
    'q0',
    alfabeto,
    {'q4'},
    {
        'q0': {'h': 'q1'},
        'q1': {'o': 'q2'},
        'q2': {'l': 'q3'},
        'q3': {'a': 'q4'},
        'q4': { alfabeto : 'q4'},         
    }
)

cadena_ejemplo = ""
with open('archivo.txt', 'r') as archivo:
    cadena_ejemplo = archivo.read()
    
if evaluar_automata(automata, cadena_ejemplo):
    print(f"La cadena '{cadena_ejemplo}' es aceptada por el autómata.")
else:
    print(f"La cadena '{cadena_ejemplo}' no es aceptada por el autómata.")
