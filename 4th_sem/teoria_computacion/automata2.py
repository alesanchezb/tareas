class Automata:
    def __init__(self, q0, F, transiciones):
        self.q0 = q0
        self.F = F
        self.transiciones = transiciones
    
    def delta(self, estado, caracter):
        if caracter not in self.transiciones[estado]:
            return estado if estado in self.F else (self.transiciones[self.q0][caracter] if caracter in self.transiciones[self.q0] else self.q0)
        return self.transiciones[estado_actual][caracter]

def evaluar_automata(automata, cadena):
    estado_actual = automata.q0
    for caracter in cadena:
        estado_actual = automata.delta(estado_actual, caracter)
    return estado_actual in automata.F

automata = Automata(
    'q0',
    {'q4'},
    {
        'q0': {'h': 'q1'},
        'q1': {'o': 'q2'},
        'q2': {'l': 'q3'},
        'q3': {'a': 'q4'},
        'q4': {'': 'q4'},         
    }
)

cadena_ejemplo = ""
with open('archivo.txt', 'r') as archivo:
    cadena_ejemplo = archivo.read()
    
if evaluar_automata(automata, cadena_ejemplo):
    print(f"La cadena '{cadena_ejemplo}' es aceptada por el autómata.")
else:
    print(f"La cadena '{cadena_ejemplo}' no es aceptada por el autómata.")
