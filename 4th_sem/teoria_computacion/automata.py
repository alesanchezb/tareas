def encontrar_palabra(texto, palabra):
    estado_actual = 0
    for caracter in texto:
        estado_actual = estado_actual + 1 if caracter == palabra[estado_actual] else 0
        if estado_actual == len(palabra):
            return True
    return False

#/*******************************************************************************************************************************************/

cadena_ejemplo = ""
with open('archivo.txt', 'r') as archivo:
    cadena_ejemplo = archivo.read()
palabra_buscada = "2022"

#print(f"La palabra '{palabra_buscada}' {'fue encontrada.' if encontrar_palabra(cadena_ejemplo, palabra_buscada) else 'no fue encontrada.'}")

#/*******************************************************************************************************************************************/
# def siguiente_estado(automata, estado_actual, simbolo):
#     return automata["transiciones"][estado_actual].get(simbolo, automata["estado_invalido"]) if estado_actual not in automata['estados_aceptacion'] else estado_actual
#/*******************************************************************************************************************************************/

def siguiente_estado(A, estado_actual, caracter):
    q0 = A['q0'];
    if caracter not in A['transiciones'][estado_actual]:
        return estado_actual if estado_actual in A['F'] else (A['transiciones'][q0][caracter] if caracter in A['transiciones'][q0] else q0)
    return A['transiciones'][estado_actual][caracter]

#/*******************************************************************************************************************************************/

def evaluar_automata(automata, cadena):
    estado_actual = automata['q0']
    estados_aceptacion = automata['F']
    for caracter in cadena:
        estado_actual = siguiente_estado(automata, estado_actual, caracter)
    return estado_actual in estados_aceptacion

#/*******************************************************************************************************************************************/

automata = {
    'q0': 'q0',
    'F': {'q4'},
    'transiciones': {
        'q0': {'h': 'q1'},
        'q1': {'o': 'q2'},
        'q2': {'l': 'q3'},
        'q3': {'a': 'q4'},
        'q4': {'': 'q4'},         
    },
}

if evaluar_automata(automata, cadena_ejemplo):
    print(f"La cadena '{cadena_ejemplo}' es aceptada por el autómata.")
else:
    print(f"La cadena '{cadena_ejemplo}' no es aceptada por el autómata.")


#/*******************************************************************************************************************************************/
