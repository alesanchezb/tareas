//-----------------------------------------------------------------------------------------------------------------------------------------------------	    	

        /*
		*Funcion que valida que esten llenos los campos de un formulario
		*@return bool
		*/
		
		function validaFormulario(formulario){
		   var valido = true;
		    // Obtemos todos los inputs text dentro del array
		   var $inputs = jQuery('#'+formulario+' :input');
		    
		    $inputs.each(function() {
			if(!isEmpty(jQuery(this).val())){ 
			  valido = false; 
			  jQuery(this).css({'background' : '#FCDBD6' , 'border' : 'solid 1px #C82C27'});
			}else{
			    jQuery(this).css( {'background' : '', 'border' : ''}); // quitamos el fondo rojo si este esta lleno
			}	
		     });
		     
		    return valido;
		}	
		
		
		/*
		* Funcion que valida que un campo sea completado
		* @retun bool
		*/
		function isEmpty(val){
		  if(jQuery.trim(val).length <= 0)
		  return false;
		  return true;
		}