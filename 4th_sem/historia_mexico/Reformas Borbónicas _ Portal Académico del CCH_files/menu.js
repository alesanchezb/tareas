(function ($) {
    Drupal.behaviors.menucch = {
        attach: function (context, settings) {	
		    /**
		    * Guardamos el estado del menu Alumno/Profesor
		    */
		     
		     var estado = $.cookie('estadoPA');
		     if ( estado == null ){
		    		$.cookie('estadoPA', '#tab1',{ expires: 1, path: '/' });
		    		estado = $.cookie('estadoPA');
		    }
		    
			 /**
			 * Funcionamiento del menu en el portal
			 */
			 
			 /*Ingresamos el la imagen del menu inicial*/
			 if ( estado == "#tab1" ){ 
				 $("#opme").removeClass("sup2");
				 $("#opme").addClass("sup1");
			 	 $("#opme").css("background-position",'0 0');
			 }else{
				 $("#opme").removeClass("sup1");
				 $("#opme").addClass("sup2");
				$("#opme").css("background-position",'0 -115px');
			 }		    
		    $(".tab_content").hide();  
		    $(estado).fadeIn("slow");
		      
		   $("ul.sup li").click(function(){  
			 		$("ul.sup li").removeClass("active");  
					$(this).addClass("active");  
					$(".tab_content").hide();  
		  			var activeTab = $(this).find("a").attr("href");
		  			if( $("ul.sup li.active a").attr("href") == "#tab1" ){
						$("#opme").removeClass("sup2");
		  			    	$("#opme").addClass("sup1");
		  			    	$("#opme").css("background-position",'0 0');
							$.cookie('estadoPA', null);
							$.cookie("estadoPA", "#tab1",{ expires: 7, path: '/' });	
		  			}else if( $("ul.sup li.active a").attr("href") == "#tab2" ){
						$("#opme").removeClass("sup1");
		  				$("#opme").addClass("sup2");
		  				$("#opme").css("background-position",'0 -115px');
						$.cookie('estadoPA', null);	
		  				$.cookie("estadoPA", "#tab2",{ expires: 1, path: '/' });
		  			}
					$(activeTab).fadeIn("slow");  
					return false;  
			});
		}
	};
}(jQuery));
