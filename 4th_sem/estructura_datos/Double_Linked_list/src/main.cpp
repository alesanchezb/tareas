#include <iostream>
#include "headers/LinkedList.hpp"
#include <chrono>
#include <thread>
#include <cstdlib> 
using namespace std;

void clearScreen() {
    system("clear");
}

int main() {
    LinkedList<int> cola;

    // int segundos = 0;
    // cout << "De 0 a cuantos segundos quieres que se genere un numero y se añada a la cola?" << endl;
    // cin >> segundos;

    // for (int i = 0; i < 100; ++i) {
    //     // Esperar el número de segundos especificado por el usuario
    //     this_thread::sleep_for(chrono::seconds(segundos));

    //     // Generar un número aleatorio entre 1 y 10
    //     int numero = rand() % 10 + 1;

    //     // Añadir el número a la cola
    //     cola.addLast(numero);
  
    //     // Limpiar la pantalla antes de imprimir la cola
    //     clearScreen();

    //     // Mostrar el número de iteración y la cola
    //     cout << "Iteración: " << i << endl;
    //     cola.print();
    // }

    cola.addFirst(1);
    cola.print();
    cola.addFirst(2);
    cola.print();
    cola.addLast(6);
    cola.print();
    cola.add(6, 1);
    cola.print();

    LinkedList<int> cola1 = cola;
    std::cout << "Lista 2: " << std::endl; 
    cola1.print(); 
    
    return 0;
}

 
