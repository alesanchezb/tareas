#include <iostream>

//****************************************************************************
template <typename T> DoubleLinkedList<T>::DoubleLinkedList() : _size(0), first(nullptr), last(nullptr) {}

//****************************************************************************
template <typename T> DoubleLinkedList<T>::~DoubleLinkedList() { clear(); }

//****************************************************************************
template <typename T>
DoubleLinkedList<T>::DoubleLinkedList(const DoubleLinkedList<T> &s) : _size(0), first(nullptr), last(nullptr) {
    *this = s;
}

//****************************************************************************
template <typename T> DoubleLinkedList<T> &DoubleLinkedList<T>::operator=(const DoubleLinkedList<T> &q) {
    if (this == &q)
        return *this;
    clear();     
    for(int i = 0; i < q.size(); ++i)         
        addLast(q.searchValue(i));
    _size = q._size;
    return *this;
}

//****************************************************************************
template <typename T> void DoubleLinkedList<T>::addFirst(T value) {
    first = new Element(value, first, first);
    if (isEmpty()) last = first;
    ++_size;
}

//****************************************************************************
template <typename T> void DoubleLinkedList<T>::addLast(T value) {
    if (isEmpty())
        first = last = new Element(value);
    else {         
        last = last->next = new Element(value);
    }
    ++_size;
}

//****************************************************************************
template <typename T> void DoubleLinkedList<T>::add(T value, int index) {
    if (index < 0 || index >= size())
        throw "Fuera de rango";
    if (index == 0)
        addFirst(value);
    else if (index == size() - 1)
        addLast(value);
    else {
        Element *after = first;
        for (int i = 1; i < index; ++i)
            after = after->next;
        Element *new_ = new Element(value, after->next);
        after->next = new_;
        ++_size;
    }     
}

//****************************************************************************
template <typename T> void DoubleLinkedList<T>::removeFirst() {
    Element *aux = first;
    first = first->next;
    delete aux;
    --_size;
}

//****************************************************************************
template <typename T> void DoubleLinkedList<T>::removeLast() {
    Element *aux = first;
    if (isEmpty())
        throw "Esta vacia";
    delete last;
    if (size() == 1) {
        first = last = nullptr;
    }else{
        for (int i = 0; i < size() - 2; ++i)
            aux = first->next;
        aux->next = nullptr;
    }
    --_size;
}
//****************************************************************************
template <typename T> void DoubleLinkedList<T>::remove(int index) {
    if (index < 0 || index >= size())
        throw "Fuera de rango";
    if (index == 0)
        removeFirst();
    else if (index == size() - 1)
        removeLast();
    else {
        Element *after = first;
        for (int i = 1; i < index; ++i)
            after = after->next;
        Element *aux = after->next;
        after->next = aux->next;
        delete aux;
        --_size;
    }     
}

//****************************************************************************
template <typename T> int DoubleLinkedList<T>::searchIndex(T value) const {
    Element *aux = first; 

    int index; 

    for(index = 0 ; aux != nullptr && aux->value != value ; ++index) 

        aux =  aux->siguiente; 

    return aux != nullptr ? index : NO_ENCONTRADO;
}

//****************************************************************************
template <typename T> T DoubleLinkedList<T>::searchValue(int index) const {
    if (index < 0 || index >= size())
        throw "Fuera de rango";

    Element *aux = first; 

    for(int i = 0 ; i < index; ++i) 

        aux =  aux->next; 

    return aux->value;
}

//****************************************************************************
template <typename T> void DoubleLinkedList<T>::modify(T v, int index) {
    if (isEmpty())
        throw "Esta vacia";
    if (index < 0 || index >= size())
        throw "Fuera de rango";
    Element *aux = first;
    for (int i = 0; i <= index; ++i)
            aux = aux->next;
    aux->value = v;
}

//****************************************************************************
template <typename T> void DoubleLinkedList<T>::clear() {
    while (!isEmpty())
        removeFirst();
}

//****************************************************************************
template <typename T> T DoubleLinkedList<T>::getFirst() const { 
    if (isEmpty())
        throw "Error: The queue is empty.";
    return first->value;
}

//****************************************************************************
template <typename T> T DoubleLinkedList<T>::getLast() const {
    if (isEmpty())
        throw "Error: The queue is empty.";
    return last->value;
}

//****************************************************************************
template <typename T> T DoubleLinkedList<T>::get(int index) const{
    if (isEmpty())
        throw "Esta vacia";
    if (index < 0 || index >= size())
        throw "Fuera de rango";
    Element *aux = first;
    for (int i = 0; i <= index; ++i)
            aux = aux->next;
    return aux->value;
}

//****************************************************************************
template <typename T> int DoubleLinkedList<T>::size() const { return _size; }

//****************************************************************************
template <typename T> bool DoubleLinkedList<T>::isEmpty() const { return _size == 0; }

//****************************************************************************
template <typename T> void DoubleLinkedList<T>::print() const {
    if (isEmpty())
        throw "Error: The queue is empty.";
    Element *aux;
    aux = first;
    while (aux != nullptr) {
        std::cout << " -> "<< aux->value;
        aux = aux->next;
    }
    std::cout << std::endl;
}
//****************************************************************************
//internal method of the queue.
template <typename T>
DoubleLinkedList<T>::Element::Element(T v, DoubleLinkedList<T>::Element *next_, DoubleLinkedList<T>::Element *after_)
: value(v), next(next_), after(after_) {}
//****************************************************************************
