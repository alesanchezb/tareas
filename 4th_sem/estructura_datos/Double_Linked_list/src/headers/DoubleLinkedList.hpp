/**
 * \file Cola.hpp
 * \brief Implementaci&oacute;n de una Matriz algebraica
 * \author Jes&uacute;s Ernesto Carrasco Ter&aacute;n
 * \author Braulio Alessandro S&aacute;nchez Berm&uacute;dez
 * \date 19/02/2024
 */


#ifndef DOUBLE_LINKED_LIST
#define DOUBLE_LINKED_LIST

#define NO_ENCONTRADO -1
/**
 * \class Queue
 * \brief Clase genérica de Queue que implementa operaciones b&aacute;sicas de
pila.
 *
 * Esta clase template ofrece una implementaci&oacute;n de pila con operaciones
para agregar,
 * eliminar elementos, y consultar el tope de la pila, entre otras.
 *
 * \tparam T Tipo de elementos almacenados en la pila.
 *
 *
 *
 * \code Ejemplo.cpp

\endverbatim
*
 */

template <typename T = int> class DoubleLinkedList {
public:
    explicit DoubleLinkedList();
    ~DoubleLinkedList();
    DoubleLinkedList(const DoubleLinkedList &);
    DoubleLinkedList &operator=(const DoubleLinkedList &);
    void addFirst(T v);
    void addLast(T v);
    void add(T v, int index);
    void removeFirst();
    void removeLast();
    void remove(int index);
    T searchValue(int index) const;
    int searchIndex(T v) const;
    bool isEmpty() const;
    T getFirst() const;
    T getLast() const;
    T get(int index) const;
    void modify(T v, int index);
    int size() const;
    void clear();
    void print() const;

private:
    int _size;
    struct Element {
        Element(T v, Element *next_ = nullptr, Element *after_ = nullptr);
        T value;
        Element *next, after;
    }* first, *last;
};

#include "../templates/DoubleLinkedList.tpp"
#endif
