#include <iostream>


int main() {
    double A[2][3];
    double B[3][4];
    double C[2][4];
    double s = 0;

    A[0][0] = 1;
    A[0][1] = 2;
    A[0][2] = 3;
    A[1][0] = 4;
    A[1][1] = 5;
    A[1][2] = 6;

    B[0][0] = 1;
    B[0][1] = 2;
    B[0][2] = 3;
    B[0][3] = 4;
    B[1][0] = 5;
    B[1][1] = 6;
    B[1][2] = 7;
    B[1][3] = 8;
    B[2][0] = 9;
    B[2][1] = 10;
    B[2][2] = 11;
    B[2][3] = 12;
    
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 4; ++j) {
            for (int k = 0; k < 3; ++k) {
                s+= A[i][k]*B[k][j];
            }
            C[i][j] = s;
            s = 0;
      }
    }

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 4; ++j) {
            std::cout << C[i][j] << ", ";
        }
        std::cout << std::endl;
    }
    return 0;
}
