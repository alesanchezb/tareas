#ifndef PILA_HPP_INCLUDED
#define PILA_HPP_INCLUDED

/*PILAS Conjunto de datos cuyas operaciones se realizan en el mismo lugar,
entran y salen los datos por el mismo lugar -> Contenedor de datos -> LIFO: Last
In First Out -> Tope (indice del ultimo elemento) vale -1 cuando no hay anda en
la pila...*/

/* \class
 * \brief
 *
 * \tparam T tipo de dato de los elementos de la pila.
 *
 */

template <typename T>

class Pila {
public:
    Pila();
    Pila(Pila& p);
    
    void push_back(T valor);                                                       
    void pop();
    T getTop() const;
    int getTam();
    void clear();                                                               
    int size() const;                                                      
    bool isEmpty() const;                                                      
    bool isFull() const;                                                      
    void print() const;                                                       
                                                                               
private:                                                                       
    int top;
    T *elemento;
    int capacity;
    void resize();
};

#include "Pila.tpp"

#endif // PILA_HPP_INCLUDED
