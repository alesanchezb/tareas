#include <iostream>

//****************
template <typename T> Pila<T>::Pila() { top = -1; }
//****************
template <typename T> Pila<T>::Pila(Pila& P) { *this = P; }
//****************
template <typename T> T Pila<T>::getTop() const {
  if (isEmpty())
    throw "...";
  return elemento[top];
}
//***************
template <typename T> bool Pila<T>::isFull() const {
  return top == cap - 1;
}
//****************
template <typename T> void Pila<T>::push_back(T val) {
  if (isFull())
    throw "...";
  elemento[++top] = val;
}
//****************
template <typename T> void Pila<T>::top() {
  if (isEmpty())
    throw "...";
  --top;
}
//***************
template <typename T> bool Pila<T>::isEmpty() const { return top == -1; }
//***************
template <typename T> int Pila<T>::getTam() const { return top + 1; }
//***************
template <typename T> void Pila<T>::clear() { top = -1; }
//**************
template <typename T> void Pila<T>::print() const {
  if (!isEmpty())
    return;
  for (int i = 0; i <= top; ++i) {
    std::cout << elemento[i] << ", ";
  }
  std::cout << "\b\b<- Top";
}
//****************
template <typename T> void resize Pila<T>::resize() {

    T *aux = new T[2 * capacity];
    for(int i = 0; i < capacity; ++i) aux[i] = elemento[i];
    delete[] elemento;
    elemento = aux;
    capacity *=2;
}
