#include "Pila.hpp"
#include <iostream>

Pila::Pila() { tope = -1; }
Pila::~Pila() {}

void Pila::agregar(int valor) { elementos[++tope] = !estaLlena() ? valor : throw "esta llena"; }
void Pila::eliminar() { !estaVacia() ? --tope : throw "esta vacia"; }
void Pila::imprimir() const {
    if(estaVacia()) return;
    for (int i = tope; i > -1; --i)
        std::cout << elementos[i] << std::endl;
}
void Pila::vaciar() { tope = -1; }
int Pila::obtenerTope() const { return !estaVacia() ? elementos[tope] : throw "..."; }
int Pila::obtenerTam() const { return tope + 1; }
bool Pila::estaLlena() const { return tope == TAM_MAX - 1; }
bool Pila::estaVacia() const { return tope == -1; }
