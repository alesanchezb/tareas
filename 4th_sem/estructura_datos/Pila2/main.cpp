// #include "Pila.hpp"
// #include <iostream>

// int main() {

//     try{
//         Pila p;

//         p.agregar(2);
//         p.agregar(8);
//         p.agregar(1);
//         p.agregar(3);

//         p.imprimir();
//         std::cout << p.obtenerTope() << ", " << p.obtenerTam() << std::endl;
//         p.eliminar();
//         std::cout << p.obtenerTope() << ", "<< p.obtenerTam() << std::endl;
//         p.eliminar();
//         std::cout << p.obtenerTope() << ", "<< p.obtenerTam() << std::endl;
//         p.eliminar();
//         std::cout << p.obtenerTope() << ", "<< p.obtenerTam() << std::endl;
//         p.eliminar();
//         p.eliminar();
//         std::cout << p.obtenerTope() << ", "<< p.obtenerTam() << std::endl;

//         std::cout << p.obtenerTope() << ", "<< p.obtenerTam() << std::endl;

//         p.imprimir();
//         p.vaciar();

//         p.agregar(2);
//         p.agregar(8);
//         p.agregar(1);
//         p.agregar(3);
//         p.agregar(1);
//         p.imprimir();

//     } catch (const char * e) {
//         std::cout << e;
//     }
//     return 0;
// }

#include "Pila.hpp"
#include <cctype>
#include <iostream>
using namespace std;

int main() {
  try {
    Pila<int> miPila;
    char respuesta;
    int valor;
    do {
      cout << "Valor a agregar: ";
      cin >> valor;
      miPila.Agregar(valor);
      cout << "Elemento agregado. Ahora la pila tiene " << miPila.ObtenerTam()
           << " elemento(s)...\n\n";
      miPila.Imprimir();
      cout << "\n\n";
      do {
        cout << "\250Quiere agregar otro elemento? (s/n): ";
        cin >> respuesta;
        respuesta = tolower(respuesta);
      } while (respuesta != 's' && respuesta != 'n');
    } while (respuesta == 's');
    cout << "Eliminando elementos..." << endl;
    do {
      cout << "Elemento a eliminar: " << miPila.ObtenerTope() << endl;
      miPila.Eliminar();
      cout << "Elemento eliminado. Ahora la pila tiene " << miPila.ObtenerTam()
           << " elemento(s)...\n\n";
      miPila.Imprimir();
      cout << "\n\n";
      do {
        cout << "\250Quiere eliminar otro elemento? (s/n): ";
        cin >> respuesta;
        respuesta = tolower(respuesta);
      } while (respuesta != 's' && respuesta != 'n');
    } while (respuesta == 's');
  } catch (const char *msn) {
    cerr << "Error: " << msn << endl;
  }
  return 0;
}
