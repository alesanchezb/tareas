#include "../headers/Expression.hpp"
#include "../headers/Stack.hpp"
#include <cmath>
Expression::Expression() : infixExp(""), postfixExp(""),isValidExp(false){
    
}
Expression::Expression(const std::string &expression) {}

void Expression::CaptureExpression() {
    getline(std::cin, infixExp);
    if(!verificarLlaves()) throw "Expresion no valida";
    ConvertExpression();
}

void Expression::ConvertExpression() {
        Stack<char> simbolos;
        std::string op = "";

        std::string str = "";
        std::string nums = "";

        int tam = infixExp.size() - 1;

        if ((verificarOp(infixExp[0]) && infixExp[0] != '-') || infixExp[0] == '.' || infixExp[0] == 'E' || infixExp[0] == 'e' ||
            infixExp[0] == '.' || verificarOp(infixExp[tam]) ||
            infixExp[tam] == 'E' || infixExp[tam] == 'e' || infixExp[tam] == '.')
            throw "Error: invalida";
        
        for (int i = 0; i < infixExp.size(); ++i) {


            if(i != tam && verificarCerrados(infixExp[i+1]) && verificarAbiertos(infixExp[i])) throw "Error: no valida";

            if (verificarAbiertos(infixExp[i])) {
                if(i != 0 && verificarCerrados(infixExp[i-1])) throw "no valida";
                simbolos.push(infixExp[i]);
            }

            if (verificarNum(infixExp[i])) {
                if(i != 0 && verificarCerrados(infixExp[i-1])) throw "no valida";
                str += infixExp[i];
            }

            if (verificarOp(infixExp[i]) && infixExp[i - 1] != 'e' &&
                infixExp[i - 1] != 'E' && infixExp[i - 1] != '.') {
                if(i != tam && verificarCerrados(infixExp[i+1])) throw "Error: no valida";
               nums += str + separator;
               if (!simbolos.isEmpty() && verificarOp(simbolos.getTop())) {
                     op += simbolos.getTop();
                     postfixExp += nums + op;
                     simbolos.pop();
                     nums = "";
                     op = "";
                }
                str = "";
                verificarJerarquia(simbolos, infixExp[i]);

            } else {
                if (infixExp[i] == '.' || infixExp[i] == 'e' || infixExp[i] == 'E' || infixExp[i] == '-' || infixExp[i] == '+') {
                  if (verificarPuntos(str + infixExp[i]))
                      throw "No valida";
                  str += infixExp[i];                   
                
                } else if (i == infixExp.size() -1 || verificarCerrados(infixExp[i])) {
                     nums += str + separator;

                    while (!simbolos.isEmpty() && regresarAbierto(infixExp[i]) != simbolos.getTop()) {
                        if (verificarOp(simbolos.getTop())) {
                            op += simbolos.getTop();
                        }
                        simbolos.pop();
                        if(!simbolos.isEmpty() && regresarAbierto(infixExp[i]) == simbolos.getTop()) simbolos.pop();
                    }

                    postfixExp += nums + op;
                    op = "";
                    nums = "";
                    str = "";
                }
            }
            
        }
}
double Expression::EvaluateExpression() {
    Stack<char> simbolos;
    Stack<double> numeros;
    
    std::string str = "";

    for (char c : postfixExp) {
        if (!verificarOp(c)) {
            if (c != separator) {
                str += c;
                continue;
            }
            if(str != "") numeros.push(std::stod(str));
            str = "";
            continue;
        }

        if(str != "")
            numeros.push(std::stod(str));
        str = "";

        double n1 = numeros.getTop();
        
        numeros.pop();

        double res = realizarOperacion(numeros.getTop(), n1, c);
        
        numeros.pop();
        numeros.push(res);

    }
    return numeros.getTop();
}
void Expression::Print(){std::cout << infixExp << std::endl;}
bool Expression::IsValid(){return isValidExp;}

bool Expression::verificarAbiertos(char caracter) {
  return caracter == '{' || caracter == '[' || caracter == '(';
}

bool Expression::verificarCerrados(char caracter) {
  return caracter == '}' || caracter == ']' || caracter == ')';
}

char Expression::regresarCierre(char abierto) {
  if (abierto == '(')
    return ')';
  if (abierto == '[')
    return ']';
  return '}';
}

char Expression::regresarAbierto(char abierto) {
    if (abierto == ')')
        return '(';
  if (abierto == ']')
      return '[';
  return '}';
}

bool Expression::verificarLlaves() {
    Stack<char> simbolos;

    bool cierre = true;

    for (int i = 0; i < infixExp.size(); ++i) {
        char caracter = infixExp[i];
        if (verificarAbiertos(caracter))
            simbolos.push(caracter);
        else if (verificarCerrados(caracter) && !simbolos.isEmpty()) {
            if (regresarCierre(simbolos.getTop()) == caracter)
                simbolos.pop();
            else 
                return false;            
        }
    }
    if (!simbolos.isEmpty())
        return false;
    return true;
}


int Expression::priority(char c) {
    switch (c) {
    case '^':
        return 3;
    case '*':
    case '/':
        return 2;
    case '+':
    case '-':
        return 1;
    default :
        return 0;
    }
}
void Expression::verificarJerarquia(Stack<char> &s, char c) {
    char top;
    if (s.isEmpty()) top = '\0';
    else top= s.getTop();

    if(verificarAbiertos(top)|| verificarAbiertos(c) || top == '\0') {
        s.push(c);
        return;
    }
    if (c == '^') {
        if (c == top || top == '*' || top == '/' || top == '+' || top == '-') {
            s.push(c);
            return;
        } 
        s.pop();
        s.push(c);
        s.push(top);
    }
    else if (c == '*' || c == '/') {
        if (c == top || top == '+' || top == '-') {
            s.push(c);
            return;
        }
        s.pop();
        s.push(c);
        s.push(top);
    } else if (c == '+' || c == '-'){
        if (c == top) {
            s.push(c);
            return;
        }
        s.pop();
        s.push(c);
        s.push(top);
    } else {
        
    }
}
bool Expression::verificarOp(char c) {
    return c == '+' || c == '-' || c == '/' || c == '*' || c == '^';
}

bool Expression::verificarNum(char c) {
    return c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8' || c == '9';
}

double Expression::realizarOperacion(double n1, double n2, char op) {
    double res = 0;
    switch (op) {
    case '+':
        res = n1 + n2;
        break;
    case '-':
        res = n1 - n2;
        break;
    case '/':
        if(n2 == 0) throw "Division por cero";
        res = n1 / n2;
        break;
    case '*':
        res = n1 * n2;
        break;
    case '^':
        std::cout << ((int)(2*n2)%2 == 0) << std::endl ;
        if (n1 == 0 && n2 < 0) {
                throw "División por cero en la operación de potencia.";
            } else if (n1 < 0 && ((int)(2*n2)%2 == 0)) {
                throw "No se puede calcular la raíz par de un número negativo.";
            }
        res = std::pow(n1, n2);
        break;
    }
  return res;
}

bool Expression::verificarPuntos(std::string s) {
    int i = 0, j = 0;
    for (char c : s) {
        i += (c == '.') ? 1 : 0;
        j += (c == 'e' || c == 'E') ? 1 : 0;
    }

    return i > 1 || j > 1;
}
