/**
 * \file Expression.hpp
 * \brief Implementaci&oacute;n de una Matriz algebraica
 * \author Ian Alonso Zepeda Godoy
 * \author Braulio Alessandro S&aacute;nchez Berm&uacute;dez
 * \date 07/03/2024
 */
#ifndef EXPRESSION_HPP
#define EXPRESSION_HPP

#include <string>
#include "../headers/Stack.hpp"

/**
 * \class Expression
 * @brief Clase para manipular expresiones matemáticas.
 */
class Expression {

public:
    
    /**
     * @brief Constructor por defecto de la clase Expression.
     */
    Expression(); //Constructor
    
    /**
     * @brief Constructor que inicializa la expresión.
     * @param expression La expresión matemática a inicializar.
     */
    Expression(const std::string &expression);
    
    /**
     * @brief Método para capturar la expresión desde la entrada.
     */
    void CaptureExpression();
    
    /**
     * @brief Método para verificar si la expresión es correcta.
     * @return True si la expresión es correcta, False en caso contrario.
     */
    bool isCorrect();
    
    /**
     * @brief Método para convertir la expresión de infija a postfija.
     */
    void ConvertExpression();
    
    /**
     * @brief Método para evaluar la expresión matemática.
     * @return El resultado de la evaluación de la expresión.
     */
    double EvaluateExpression();
    
    /**
     * @brief Método para imprimir la expresión.
     */
    void Print();
    
    /**
     * @brief Método para verificar si la expresión es válida.
     * @return True si la expresión es válida, False en caso contrario.
     */
    bool IsValid();

private:
    
    std::string infixExp; ///< Expresión infija.
    std::string postfixExp; ///< Expresión postfija.
    bool isValidExp; ///< Indica si la expresión es válida o no.

    const char separator = ';'; ///< Separador de expresiones.

    /**
     * @brief Determina la prioridad de un operador.
     * @param c El operador a evaluar.
     * @return El nivel de prioridad del operador.
     */
    int priority(char c);
    
    /**
     * @brief Verifica la jerarquía de los operadores en la pila.
     * @param s La pila de operadores.
     * @param c El operador actual.
     */
    void verificarJerarquia(Stack<char> &s, char c);
    
    /**
     * @brief Verifica si un caracter es un operador válido.
     * @param c El caracter a verificar.
     * @return True si el caracter es un operador, False en caso contrario.
     */
    bool verificarOp(char c);
    
    /**
     * @brief Verifica si un caracter es un número válido.
     * @param c El caracter a verificar.
     * @return True si el caracter es un número, False en caso contrario.
     */
    bool verificarNum(char c);
    
    /**
     * @brief Realiza una operación aritmética.
     * @param n1 Primer número.
     * @param n2 Segundo número.
     * @param op Operador.
     * @return El resultado de la operación.
     */
    double realizarOperacion(double n1, double n2, char op);
    
    /**
     * @brief Verifica si la expresión contiene puntos decimales válidos.
     * @param s La expresión a verificar.
     * @return True si la expresión tiene puntos decimales válidos, False en caso contrario.
     */
    bool verificarPuntos(std::string s);
    
    /**
     * @brief Verifica si hay paréntesis abiertos.
     * @param caracter El caracter a verificar.
     * @return True si hay paréntesis abiertos, False en caso contrario.
     */
    bool verificarAbiertos(char caracter);
    
    /**
     * @brief Verifica si hay llaves abiertas.
     * @return True si hay llaves abiertas, False en caso contrario.
     */
    bool verificarLlaves();
    
    /**
     * @brief Verifica si hay paréntesis cerrados.
     * @param caracter El caracter a verificar.
     * @return True si hay paréntesis cerrados, False en caso contrario.
     */
    bool verificarCerrados(char caracter);
    
    /**
     * @brief Retorna el carácter de cierre correspondiente a uno de apertura.
     * @param abierto El caracter de apertura.
     * @return El caracter de cierre correspondiente.
     */
    char regresarCierre(char abierto);
    
    /**
     * @brief Retorna el carácter de apertura correspondiente a uno de cierre.
     * @param abierto El caracter de cierre.
     * @return El caracter de apertura correspondiente.
     */
    char regresarAbierto(char abierto);
};

#endif
