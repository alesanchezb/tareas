#include <iostream>

//****************************************************************************
template <typename T> LinkedList<T>::LinkedList() : _size(0), first(nullptr), last(nullptr) {}

//****************************************************************************
template <typename T> LinkedList<T>::~LinkedList() { clear(); }

//****************************************************************************
template <typename T>
LinkedList<T>::LinkedList(const LinkedList<T> &s) : _size(0), first(nullptr), last(nullptr) {
    *this = s;
}

//****************************************************************************
template <typename T> LinkedList<T> &LinkedList<T>::operator=(const LinkedList<T> &q) {
    if (this == &q)
        return *this;
    clear();     
    for(int i = 0; i < q.size(); ++i)         
        addLast(q.searchValue(i));
    _size = q._size;
    return *this;
}

//****************************************************************************
template <typename T> void LinkedList<T>::addFirst(T value) {
    first = new Element(value, first);
    if (isEmpty()) last = first;
    ++_size;
}

//****************************************************************************
template <typename T> void LinkedList<T>::addLast(T value) {
    if (isEmpty())
        first = last = new Element(value);
    else {         
        last = last->next = new Element(value);
    }
    ++_size;
}

//****************************************************************************
template <typename T> void LinkedList<T>::add(T value, int index) {
    if (index < 0 || index >= size())
        throw "Fuera de rango";
    if (index == 0)
        addFirst(value);
    else if (index == size() - 1)
        addLast(value);
    else {
        Element *after = first;
        for (int i = 1; i < index; ++i)
            after = after->next;
        Element *new_ = new Element(value, after->next);
        after->next = new_;
        ++_size;
    }     
}

//****************************************************************************
template <typename T> void LinkedList<T>::removeFirst() {
    Element *aux = first;
    first = first->next;
    delete aux;
    --_size;
}

//****************************************************************************
template <typename T> void LinkedList<T>::removeLast() {
    Element *aux = first;
    if (isEmpty())
        throw "Esta vacia";
    delete last;
    if (size() == 1) {
        first = last = nullptr;
    }else{
        for (int i = 0; i < size() - 2; ++i)
            aux = first->next;
        aux->next = nullptr;
    }
    --_size;
}
//****************************************************************************
template <typename T> void LinkedList<T>::remove(int index) {
    if (index < 0 || index >= size())
        throw "Fuera de rango";
    if (index == 0)
        removeFirst();
    else if (index == size() - 1)
        removeLast();
    else {
        Element *after = first;
        for (int i = 1; i < index; ++i)
            after = after->next;
        Element *aux = after->next;
        after->next = aux->next;
        delete aux;
        --_size;
    }     
}

//****************************************************************************
template <typename T> int LinkedList<T>::searchIndex(T value) const {
    Element *aux = first; 

    int index; 

    for(index = 0 ; aux != nullptr && aux->value != value ; ++index) 

        aux =  aux->siguiente; 

    return aux != nullptr ? index : NO_ENCONTRADO;
}

//****************************************************************************
template <typename T> T LinkedList<T>::searchValue(int index) const {
    if (index < 0 || index >= size())
        throw "Fuera de rango";

    Element *aux = first; 

    for(int i = 0 ; i < index; ++i) 

        aux =  aux->next; 

    return aux->value;
}

//****************************************************************************
template <typename T> void LinkedList<T>::modify(T v, int index) {
    if (isEmpty())
        throw "Esta vacia";
    if (index < 0 || index >= size())
        throw "Fuera de rango";
    Element *aux = first;
    for (int i = 0; i <= index; ++i)
            aux = aux->next;
    aux->value = v;
}

//****************************************************************************
template <typename T> void LinkedList<T>::clear() {
    while (!isEmpty())
        removeFirst();
}

//****************************************************************************
template <typename T> T LinkedList<T>::getFirst() const { 
    if (isEmpty())
        throw "Error: The queue is empty.";
    return first->value;
}

//****************************************************************************
template <typename T> T LinkedList<T>::getLast() const {
    if (isEmpty())
        throw "Error: The queue is empty.";
    return last->value;
}

//****************************************************************************
template <typename T> T LinkedList<T>::get(int index) const{
    if (isEmpty())
        throw "Esta vacia";
    if (index < 0 || index >= size())
        throw "Fuera de rango";
    Element *aux = first;
    for (int i = 0; i <= index; ++i)
            aux = aux->next;
    return aux->value;
}

//****************************************************************************
template <typename T> int LinkedList<T>::size() const { return _size; }

//****************************************************************************
template <typename T> bool LinkedList<T>::isEmpty() const { return _size == 0; }

//****************************************************************************
template <typename T> void LinkedList<T>::print() const {
    if (isEmpty())
        throw "Error: The queue is empty.";
    Element *aux;
    aux = first;
    while (aux != nullptr) {
        std::cout << " -> "<< aux->value;
        aux = aux->next;
    }
    std::cout << std::endl;
}
//****************************************************************************
//internal method of the queue.
template <typename T>
LinkedList<T>::Element::Element(T v, LinkedList<T>::Element *next_)
: value(v), next(next_) {}
//****************************************************************************
