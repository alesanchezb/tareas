/**
 * \file Pila.hpp
 * \brief Implementaci&oacute;n de una Matriz algebraica
 * \author Jes&uacute;s Ernesto Carrasco Ter&aacute;n
 * \author Braulio Alessandro S&aacute;nchez Berm&uacute;dez
 * \date 19/02/2024
 */

#ifndef PILA_HPP
#define PILA_HPP

/**
 * \class Pila
 * \brief Clase genérica de Pila que implementa operaciones b&aacute;sicas de
pila.
 *
 * Esta clase template ofrece una implementaci&oacute;n de pila con operaciones
para agregar,
 * eliminar elementos, y consultar el tope de la pila, entre otras.
 *
 * \tparam T Tipo de elementos almacenados en la pila.
 *
 *
 *
 * \code Ejemplo.cpp
#include "headers/Pila.hpp"
#include <iostream>
#include <string>
#include "headers/Matriz.hpp"

using namespace std;

void test_uno() {
    cout << "Prueba 1 \n";
    Pila<int> miPila;
    cout << "Llenar pila: \n";

    for (int i = 0; i < 15; ++i) {
      miPila.Agregar(i);
    }

    cout << "\nimprimiendo pila\n";
    miPila.Imprimir();

    std::cout << "\ntam: " << miPila.ObtenerTam()
    << " capacidad: " << miPila.ObtenerCap() << std::endl;

    cout << "\nCopiando la pila\n";
    Pila<int> otraPila = miPila;

    cout << "\nImprimiendo la pila copiada\n";
    otraPila.Imprimir();

    cout << "\nEliminando elementos de la copia\n";
    otraPila.Eliminar();

    cout << "\nImprimiendo pila copiada\n";
    otraPila.Imprimir();

    std::cout << "\nEl tope de la primera pila es: " << miPila.ObtenerTope()
              << std::endl;
    std::cout << "El tope de la segunda pila es: " << otraPila.ObtenerTope()
              << std::endl;
    // Prueba Vaciar
    cout << "\nVaciar la pila";
    miPila.Vaciar();
    cout << "\nPila vaciada: ";
    miPila.Imprimir();
    // Prueba EstaVacia
    if (miPila.EstaVacia())
      std::cout << "La primera pila esta vacia." << std::endl;

}

void test_dos() {
    cout << "\nPrueba 2 \n";
    Pila<string> miPila;
    cout << "Llenar pila: \n";

    for (int i = 0; i < 15; ++i) {
      miPila.Agregar(to_string(i));
    }

    cout << "\nimprimiendo pila\n";
    miPila.Imprimir();

    std::cout << "\ntam: " << miPila.ObtenerTam()
    << " capacidad: " << miPila.ObtenerCap() << std::endl;

    cout << "\nCopiando la pila";
    Pila<string> otraPila = miPila;

    cout << "\nImprimiendo la pila copiada\n";
    otraPila.Imprimir();

    cout << "\nEliminando elementos de la copia\n";
    otraPila.Eliminar();

    cout << "\nImprimiendo pila copiada\n";
    otraPila.Imprimir();

    std::cout << "\nEl tope de la primera pila es: " << miPila.ObtenerTope()
              << std::endl;
    std::cout << "El tope de la segunda pila es: " << otraPila.ObtenerTope()
              << std::endl;
    // Prueba Vaciar
    cout << "\nVaciar la pila";
    miPila.Vaciar();
    cout << "\nPila vaciada: ";
    miPila.Imprimir();
    // Prueba EstaVacia
    if (miPila.EstaVacia())
        std::cout << "La primera pila esta vacia." << std::endl;
}

void test_tres() {
    cout << "\nPrueba 3 \n";
    Pila<Matriz> miPila;
    cout << "Llenar pila: \n";

    Matriz m1(2,2), m2(3,3), m3;

    miPila.Agregar(m1);
    miPila.Agregar(m2);
    miPila.Agregar(m3);

    cout << "\nimprimiendo pila\n";
    miPila.Imprimir();

    std::cout << "\ntam: " << miPila.ObtenerTam()
    << " capacidad: " << miPila.ObtenerCap() << std::endl;

    cout << "\nCopiando la pila";
    Pila<Matriz> otraPila = miPila;

    cout << "\nImprimiendo la pila copiada\n";
    otraPila.Imprimir();

    cout << "\nEliminando elementos de la copia\n";
    otraPila.Eliminar();

    cout << "\nImprimiendo pila copiada\n";
    otraPila.Imprimir();

    std::cout << "\nEl tope de la primera pila es: " << miPila.ObtenerTope()
    << std::endl;
    std::cout << "\nEl tope de la segunda pila es: " << otraPila.ObtenerTope()
    << std::endl;

    cout << "\nVaciar la pila";
    miPila.Vaciar();
    cout << "\nPila vaciada: ";
    miPila.Imprimir();

    if (miPila.EstaVacia())
        std::cout << "\nLa primera pila esta vacia." << std::endl;
}

int main() {
  try {
      test_uno();

      test_dos();

      test_tres();
  } catch (std::bad_alloc &) {
  } catch (const char *&) {
      cerr << "Error";
  }
  return 0;
}
 * \endcode
 *
 \verbatim Salida:
 Prueba 1
Llenar pila:

imprimiendo pila
0 <- 1 <- 2 <- 3 <- 4 <- 5 <- 6 <- 7 <- 8 <- 9 <- 10 <- 11 <- 12 <- 13 <- 14<-
tope tam: 15 capacidad: 20

Copiando la pila

Imprimiendo la pila copiada
0 <- 1 <- 2 <- 3 <- 4 <- 5 <- 6 <- 7 <- 8 <- 9 <- 10 <- 11 <- 12 <- 13 <- 14<-
tope Eliminando elementos de la copia

Imprimiendo pila copiada
0 <- 1 <- 2 <- 3 <- 4 <- 5 <- 6 <- 7 <- 8 <- 9 <- 10 <- 11 <- 12 <- 13 <-<- tope
El tope de la primera pila es: 14
El tope de la segunda pila es: 13

Vaciar la pila
Pila vaciada: La primera pila esta vacia.

Prueba 2
Llenar pila:

imprimiendo pila
0 <- 1 <- 2 <- 3 <- 4 <- 5 <- 6 <- 7 <- 8 <- 9 <- 10 <- 11 <- 12 <- 13 <- 14<-
tope tam: 15 capacidad: 20

Copiando la pila
Imprimiendo la pila copiada
0 <- 1 <- 2 <- 3 <- 4 <- 5 <- 6 <- 7 <- 8 <- 9 <- 10 <- 11 <- 12 <- 13 <- 14<-
tope Eliminando elementos de la copia

Imprimiendo pila copiada
0 <- 1 <- 2 <- 3 <- 4 <- 5 <- 6 <- 7 <- 8 <- 9 <- 10 <- 11 <- 12 <- 13 <-<- tope
El tope de la primera pila es: 14
El tope de la segunda pila es: 13

Vaciar la pila
Pila vaciada: La primera pila esta vacia.

Prueba 3
Llenar pila:

imprimiendo pila
[0][0], [0][0] <- [0][0][0], [0][0][0], [0][0][0] <- [0]<- tope
tam: 3 capacidad: 5

Copiando la pila
Imprimiendo la pila copiada
[0][0], [0][0] <- [0][0][0], [0][0][0], [0][0][0] <- [0]<- tope
Eliminando elementos de la copia

Imprimiendo pila copiada
[0][0], [0][0] <- [0][0][0], [0][0][0], [0][0][0]<- tope
El tope de la primera pila es: [0],

El tope de la segunda pila es: [0][0][0], [0][0][0], [0][0][0],

Vaciar la pila
Pila vaciada:
La primera pila esta vacia.
\endverbatim
*
 */
template <typename T = int>
class Pila
{
    public:
        /**
         * \brief Constructor por defecto que inicializa una pila vac&iacute;a.
         */
        explicit Pila();

        /**
         * \brief Constructor de copia.
         * \param p Otra pila de la cual se copiar&aacute;n los elementos.
         */
        Pila(const Pila &p);

        /**
         * \brief Sobrecarga del operador de asignaci&oacute;n.
         * \param p Otra pila de la cual se copiar&aacute;n los elementos.
         * \exception const <b>char *</b> La pila copia no puede ser creado.
         *
         * \return Referencia a la pila actual.
         */
        Pila & operator=(const Pila &p);

        /**
         * \brief Destructor que libera la memoria de la pila.
         */
        ~Pila();

        /**
         * \brief Agrega un nuevo elemento al tope de la pila.
         * \param valor El valor a agregar.
         */
        void Agregar(T valor);

        /**
         * \brief Elimina el elemento del tope de la pila.
         */
        void Eliminar();

        /**
         * \brief Obtiene el valor del tope de la pila sin eliminarlo.
         *
         * \exception const <b>char *</b> La pila está vacía.

         * \return El valor en el tope de la pila.
         */
        T ObtenerTope() const;

        /**
         * \brief Vac&iacute;a la pila eliminando todos sus elementos.
         *
         * \exception const <b>char *</b> La pial está vacía.

         */
        void Vaciar();

        /**
         * \brief Obtiene la capacidad actual de la pila.
         * \return La capacidad de la pila.
         */
        int ObtenerCap() const;

        /**
         * \brief Obtiene el tamaño actual de la pila, es decir, el n&uacute;mero de elementos.
         * \return El n&uacute;mero de elementos en la pila.
         */
        int ObtenerTam() const;

        /**
         * \brief Verifica si la pila est&aacute; vac&iacute;a.
         * \return Verdadero si la pila est&aacute; vac&iacute;a, falso de lo contrario.
         */
        bool EstaVacia() const;

        /**
         * \brief Verifica si la pila est&aacute; llena.
         * \return Verdadero si la pila est&aacute; llena, falso de lo contrario.
         */
        bool EstaLlena() const;

        /**
         * \brief Imprime todos los elementos de la pila, utilizado para pruebas.
         */
        void Imprimir() const;

    private:
        int tope; //!< &Iacute;ndice del tope de la pila
        T *elemento; //!< Array din&aacute;mico que almacena los elementos de la pila
        int cap; //!< Capacidad actual del array din&aacute;mico

        /**
         * \brief Redimensiona el array din&aacute;mico que almacena los elementos de la pila.
         */
        void Redimensionar();
};

#include "../templates/Pila.tpp"

#endif // PILA_HPP

