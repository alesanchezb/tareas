#include <iostream>

template <typename T/*= int*/>
Pila<T>::Pila(): cap(5)
{
    //ctor
     tope = -1;
    elemento = new T[cap];
}
//**************************************************
template <typename T/*= int*/>
Pila<T>::Pila(const Pila &p): elemento(nullptr) //constructo de copias
{
    *this = p;
}
//**************************************************
template <typename T/*= int*/>
Pila<T>::~Pila() //destructor
{
    delete []elemento;
}
//**************************************************
template <typename T/*= int*/>
Pila<T> & Pila<T>::operator=(const Pila &p) //operador =
{
    //ctor
    if(this == &p) return *this;
    try{
        cap = p.cap;
        tope = p.tope;
        elemento = new T[cap];

        for(int i = 0; i <= tope; ++i){
                elemento[i] = p.elemento[i];
        }
    }catch(std::bad_alloc &){
         throw "No es posible construir la pila";
    }
    return *this;
}
//**************************************************
template <typename T/*= int*/>
void Pila<T>::Agregar(T valor)
{
    if(EstaLlena()) Redimensionar();
    elemento[++tope] = valor;
}
//**************************************************
template <typename T/*= int*/>
void Pila<T>::Eliminar()
{
    if(EstaVacia()) throw "La pila esta vacia";
    --tope;
}
//**************************************************
template <typename T/*= int*/>
T Pila<T>::ObtenerTope() const
{
    if(EstaVacia()) throw "Esta vacio";
    return elemento[tope];
}
//**************************************************
template <typename T/*= int*/>
void Pila<T>::Vaciar()
{
    tope = -1;

}
//**************************************************
template <typename T/* = int*/>
int Pila<T>::ObtenerCap() const {
    return cap;
}
//**************************************************
template <typename T/*= int*/>
int Pila<T>::ObtenerTam() const
{
    return tope +1;
}
//**************************************************
template <typename T/*= int*/>
bool Pila<T>::EstaVacia() const
{
    return tope == -1;
}
//**************************************************
template <typename T/*= int*/>
bool Pila<T>::EstaLlena() const
{
    return tope == cap-1;
}
//**************************************************
template <typename T/*= int*/>
void Pila<T>::Imprimir() const
{
    if(EstaVacia()) return;
    for(int i=0; i<=tope; ++i)
    {
        std::cout<< elemento[i]<< " <- ";
    }
    std::cout<<"\b\b\b\b<- tope";
}
//**************************************************
// metodo de utileria
template <typename T/*= int*/>
void Pila<T>::Redimensionar()
{
    T *aux= elemento;
    cap *= 2;
    elemento = new T[cap];
    for(int i=0; i<=tope; ++i)
    {
        elemento[i] = aux[i];
    }
    delete []aux;
}
