#include "headers/Pila.hpp"
#include <iostream>
#include <string>
#include "headers/Matriz.hpp"
 
using namespace std;

void test_uno() {
    cout << "Prueba 1 \n";
    Pila<int> miPila;
    cout << "Llenar pila: \n";

    for (int i = 0; i < 15; ++i) {
      miPila.Agregar(i);
    }

    cout << "\nimprimiendo pila\n";
    miPila.Imprimir();

    std::cout << "\ntam: " << miPila.ObtenerTam()
    << " capacidad: " << miPila.ObtenerCap() << std::endl;

    cout << "\nCopiando la pila\n";
    Pila<int> otraPila = miPila;

    cout << "\nImprimiendo la pila copiada\n";
    otraPila.Imprimir();

    cout << "\nEliminando elementos de la copia\n";
    otraPila.Eliminar();

    cout << "\nImprimiendo pila copiada\n";
    otraPila.Imprimir();

    std::cout << "\nEl tope de la primera pila es: " << miPila.ObtenerTope()
              << std::endl;
    std::cout << "El tope de la segunda pila es: " << otraPila.ObtenerTope()
              << std::endl;
    // Prueba Vaciar
    cout << "\nVaciar la pila";
    miPila.Vaciar();
    cout << "\nPila vaciada: ";
    miPila.Imprimir();
    // Prueba EstaVacia
    if (miPila.EstaVacia())
      std::cout << "La primera pila esta vacia." << std::endl;

}

void test_dos() {
    cout << "\nPrueba 2 \n";
    Pila<string> miPila;
    cout << "Llenar pila: \n";

    for (int i = 0; i < 15; ++i) {
      miPila.Agregar(to_string(i));
    }

    cout << "\nimprimiendo pila\n";
    miPila.Imprimir();

    std::cout << "\ntam: " << miPila.ObtenerTam()
    << " capacidad: " << miPila.ObtenerCap() << std::endl;

    cout << "\nCopiando la pila";
    Pila<string> otraPila = miPila;

    cout << "\nImprimiendo la pila copiada\n";
    otraPila.Imprimir();

    cout << "\nEliminando elementos de la copia\n";
    otraPila.Eliminar();

    cout << "\nImprimiendo pila copiada\n";
    otraPila.Imprimir();

    std::cout << "\nEl tope de la primera pila es: " << miPila.ObtenerTope()
              << std::endl;
    std::cout << "El tope de la segunda pila es: " << otraPila.ObtenerTope()
              << std::endl;
    // Prueba Vaciar
    cout << "\nVaciar la pila";
    miPila.Vaciar();
    cout << "\nPila vaciada: ";
    miPila.Imprimir();
    // Prueba EstaVacia
    if (miPila.EstaVacia())
        std::cout << "La primera pila esta vacia." << std::endl;
}

void test_tres() {
    cout << "\nPrueba 3 \n";
    Pila<Matriz> miPila;
    cout << "Llenar pila: \n";

    Matriz m1(2,2), m2(3,3), m3;

    miPila.Agregar(m1);
    miPila.Agregar(m2);
    miPila.Agregar(m3);
    
    cout << "\nimprimiendo pila\n";
    miPila.Imprimir();

    std::cout << "\ntam: " << miPila.ObtenerTam()
    << " capacidad: " << miPila.ObtenerCap() << std::endl;

    cout << "\nCopiando la pila";
    Pila<Matriz> otraPila = miPila;

    cout << "\nImprimiendo la pila copiada\n";
    otraPila.Imprimir();

    cout << "\nEliminando elementos de la copia\n";
    otraPila.Eliminar();

    cout << "\nImprimiendo pila copiada\n";
    otraPila.Imprimir();
    
    std::cout << "\nEl tope de la primera pila es: " << miPila.ObtenerTope()
    << std::endl;
    std::cout << "\nEl tope de la segunda pila es: " << otraPila.ObtenerTope()
    << std::endl;

    cout << "\nVaciar la pila";
    miPila.Vaciar();
    cout << "\nPila vaciada: ";
    miPila.Imprimir();

    if (miPila.EstaVacia())
        std::cout << "\nLa primera pila esta vacia." << std::endl;
}

int main() {

  try {
      test_uno();

      test_dos();

      test_tres();
  } catch (std::bad_alloc &) {
  } catch (const char *&) {
      cerr << "Error";
  }
  return 0;
}
