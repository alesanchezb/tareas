#ifndef VECTOR_H
#define VECTOR_H

#include <istream>
/*
 * Si al menos uno de los atributos de una clase se genera en memoria dinámica
 (usa memoria del heap) es obligatorio para asegurar el buen funcionanmiento de
 la clase escribir los siguientes métodos:
 * 1. Constructor por default
 * 2. Constructor de copias
 * 3. Operador de asiganación (=)
 * 4. Destructor
 */
class Vector
{
    friend std::ostream& operator<<(std::ostream &out, Vector& v);
    
    friend std::istream& operator>>(std::istream& in, Vector& v);
public:
    explicit Vector(int dim = 3);
    ~Vector();

    Vector(Vector &v);

    Vector operator+(const Vector& v) const;
    Vector& operator=(const Vector &v);
    
    void setDimension(int dim);
    void capture();
    void print(std::ostream& out) const;

    int getDimension() const;
private:
    int dim;
    double* component;
};

#endif
