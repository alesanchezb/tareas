#include <cctype>
#include <iostream>

#include "Pila.hpp"
using namespace std;

int main() {
  try {
    // Pila<int> miPila;
    Pila<int> miPila;

    std::cout << " capacidad: " << miPila.ObtenerCap() << std::endl;
    for (int i = 0; i < 10; ++i) {
      miPila.Agregar(i);
      cout << endl;
      miPila.Imprimir();
      cout << endl;
    }

    cout << "tama�o: " << miPila.ObtenerTam()
         << " capacidad: " << miPila.ObtenerCap() << endl;

    cout << "constructor de copias " << endl;
    Pila<int> otraPila = miPila;
    otraPila.Imprimir();
    cout << endl;
    cout << "tama�o: " << otraPila.ObtenerTam()
         << " capacidad: " << otraPila.ObtenerCap() << endl;

  } catch (const char *msn) {
    cerr << "Error: " << msn << endl;
  }

  return 0;
}
