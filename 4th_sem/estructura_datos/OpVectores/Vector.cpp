#include <iostream>
#include <iterator>
#include "../headers/Vector.hpp"

//***************************************************************

Vector::Vector(int dim /*= 3*/) {
    setDimension(dim);
    component = new double[dim];
}

//***************************************************************

Vector::Vector(Vector &v) {
    setDimension(v.getDimension());
    component = new double[dim];
    for (int i = 0; i < dim; ++i) {
        component[i] = v.component[i];
    }
}

//***************************************************************

Vector::~Vector() {
    delete[] component;
}

//***************************************************************

void Vector::setDimension(int dim) {
    if (dim < 1) throw "Valor fuera de rango";
    this->dim = dim;
}

//***************************************************************

int Vector::getDimension() const{ return this->dim; }

//***************************************************************

void Vector::capture() {
    for (int i = 0; i < this->dim; ++i) {
        std::cin >> component[i];
  }
}

//***************************************************************

void Vector::print(std::ostream &out) const{
    out << "(";
    for (int i = 0; i < this->dim; ++i) {
        out << this->component[i] << ", ";
    }
    out << "\b\b)";
}

//***************************************************************

std::ostream& operator<<(std::ostream& out, Vector& v) {
    v.print(out);
    return out;
}

//***************************************************************

std::istream& operator>>(std::istream& in, Vector& v) {
    for(int i = 0; i < v.getDimension(); ++i){
        in >> v.component[i];
    }
    return in;
}

//***************************************************************

Vector Vector::operator+(const Vector& v) const{   
    if (dim != v.getDimension())
        throw "Dimesion";
    
    Vector suma(v.getDimension());
    for (int i = 0; i < v.getDimension(); ++i) {
        suma.component[i] = v.component[i] + this->component[i];
    }
    return suma;
}

//***************************************************************

Vector& Vector::operator=(const Vector &v) {
    std::cout << &v << std::endl;
    if(this == &v) return *this;
    if(dim != v.getDimension()){ 
        setDimension(v.getDimension());
        delete [] component;
        component = new double[dim];
    }
    for (int i = 0; i < dim; ++i) {
        component[i] = v.component[i];
    }
    return *this;
}

//***************************************************************
