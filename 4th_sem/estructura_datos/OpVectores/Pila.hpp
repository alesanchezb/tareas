/**
 * \file Pila.hpp
 * \brief Implementaci&oacute;n de una Matriz algebraica
 * \author Jes&uacute;s Ernesto Carrasco Ter&aacute;n
 * \author Braulio Alessandro S&aacute;nchez Berm&uacute;dez
 * \date 19/02/2024
 */

#ifndef PILA_HPP
#define PILA_HPP

/**
 * \class Pila
 * \brief Clase genérica de Pila que implementa operaciones b&aacute;sicas de pila.
 *
 * Esta clase template ofrece una implementaci&oacute;n de pila con operaciones para agregar,
 * eliminar elementos, y consultar el tope de la pila, entre otras.
 * 
 * \tparam T Tipo de elementos almacenados en la pila.
 */
template <typename T = int>
class Pila
{
    public:
        /**
         * \brief Constructor por defecto que inicializa una pila vac&iacute;a.
         */
        Pila();

        /**
         * \brief Constructor de copia.
         * \param p Otra pila de la cual se copiar&aacute;n los elementos.
         */
        Pila(const Pila &p);

        /**
         * \brief Sobrecarga del operador de asignaci&oacute;n.
         * \param p Otra pila de la cual se copiar&aacute;n los elementos.
         * \return Referencia a la pila actual.
         */
        Pila & operator=(const Pila &p);

        /**
         * \brief Destructor que libera la memoria de la pila.
         */
        ~Pila();

        /**
         * \brief Agrega un nuevo elemento al tope de la pila.
         * \param valor El valor a agregar.
         */
        void Agregar(T valor);

        /**
         * \brief Elimina el elemento del tope de la pila.
         */
        void Eliminar();

        /**
         * \brief Obtiene el valor del tope de la pila sin eliminarlo.
         * \return El valor en el tope de la pila.
         */
        T ObtenerTope() const;

        /**
         * \brief Vac&iacute;a la pila eliminando todos sus elementos.
         */
        void Vaciar();

        /**
         * \brief Obtiene la capacidad actual de la pila.
         * \return La capacidad de la pila.
         */
        int ObtenerCap() const;

        /**
         * \brief Obtiene el tamaño actual de la pila, es decir, el n&uacute;mero de elementos.
         * \return El n&uacute;mero de elementos en la pila.
         */
        int ObtenerTam() const;

        /**
         * \brief Verifica si la pila est&aacute; vac&iacute;a.
         * \return Verdadero si la pila est&aacute; vac&iacute;a, falso de lo contrario.
         */
        bool EstaVacia() const;

        /**
         * \brief Verifica si la pila est&aacute; llena.
         * \return Verdadero si la pila est&aacute; llena, falso de lo contrario.
         */
        bool EstaLlena() const;

        /**
         * \brief Imprime todos los elementos de la pila, utilizado para pruebas.
         */
        void Imprimir() const;

    private:
        int tope; //!< &Iacute;ndice del tope de la pila
        T *elementos; //!< Array din&aacute;mico que almacena los elementos de la pila
        int capacidad; //!< Capacidad actual del array din&aacute;mico

        /**
         * \brief Redimensiona el array din&aacute;mico que almacena los elementos de la pila.
         */
        void Redimensionar();
};

#include "Pila.tpp"

#endif // PILA_HPP
